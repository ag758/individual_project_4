# IDS721 Individual Project 4

## Requirements
Rust AWS Lambda and Step Functions
- Rust AWS Lambda function
- Step Functions workflow coordinating Lambdas
- Orchestrate data processing pipeline
# IDS721 Individual Project 4
Since the project was more than 500 MB, the code couldn't be pushed to gitlab and the snap of local is provided for reference. 
## Demo Video
[Demo Video](https://www.youtube.com/watch?v=S1sDtkV11Fg).

## Requirements
Rust AWS Lambda and Step Functions
- Rust AWS Lambda function
- Step Functions workflow coordinating Lambdas
- Orchestrate data processing pipeline
## Rust Lambda Functionality
The process_init function will produce a JSON message indicating the completion of data preparation, stating "Data Preparation Complete" and "Data is ready for the next step". Subsequently, the process_data function will receive this JSON message as input, proceeding to generate a new JSON message confirming the completion of data processing, with the message "Processing Complete" and "Data has been processed".

![alt text](assets/code.png)



## Project Steps
### Setting Up Rust Lambda Project
1. Begin by creating a new Lambda project using the command cargo lambda new <PROJECT_NAME>.
2. Add essential dependencies to the Cargo.toml file.
3. Implement the desired functionalities within the main.rs file.
4. Follow the same process to create additional Lambda functions if needed.


### Local Testing

1. Use `cargo lambda watch` to test the lambda functions locally.
2. Use `cargo lambda invoke --data-ascii '{}'` to see the resulting JSON object. 

### Deploy on AWS Lambda
1. Create a role and add the following dependencies to the role: `AWSLambda_FullAccess`, `AWSLambdaBasicExecutionRole`, `IAMFullAccess` to ensure proper deployment and usage of the lambda functions.

2. Build the lambda function using the following command:
    ```
    cargo lambda build --release
    ```
3. Deploy the lambda function to AWS Lambda using the following command:
    ```
    cargo lambda deploy --region <REGION> --iam-role <ROLE_ARN>
    ```
4. Repeat the above steps to deploy the second lambda function. 

### Build Corresponding Step Functions
1. Create a new State Machine in AWS Step Functions. 
2. Implement the workflow of the step function by choosing AWS Lambda Invoke and dragging it to the state machine with the order of the two functions. An example of a resulting State machine is as follows: 
    ```json
    {
        "Comment": "A description of my state machine",
        "StartAt": "process_init",
        "States": {
            "process_init": {
            "Type": "Task",
            "Resource": "arn:aws:states:::lambda:invoke",
            "OutputPath": "$.Payload",
            "Parameters": {
                "Payload.$": "$",
                "FunctionName": "arn:aws:lambda:us-east-1:533267197894:function:process_init:$LATEST"
            },
            "Retry": [
                {
                "ErrorEquals": [
                    "Lambda.ServiceException",
                    "Lambda.AWSLambdaException",
                    "Lambda.SdkClientException",
                    "Lambda.TooManyRequestsException"
                ],
                "IntervalSeconds": 1,
                "MaxAttempts": 3,
                "BackoffRate": 2
                }
            ],
            "Next": "process_data"
            },
            "process_data": {
            "Type": "Task",
            "Resource": "arn:aws:states:::lambda:invoke",
            "OutputPath": "$.Payload",
            "Parameters": {
                "Payload.$": "$",
                "FunctionName": "arn:aws:lambda:us-east-1:533267197894:function:process_data:$LATEST"
            },
            "Retry": [
                {
                "ErrorEquals": [
                    "Lambda.ServiceException",
                    "Lambda.AWSLambdaException",
                    "Lambda.SdkClientException",
                    "Lambda.TooManyRequestsException"
                ],
                "IntervalSeconds": 1,
                "MaxAttempts": 3,
                "BackoffRate": 2
                }
            ],
            "End": true
            }
        }
    }
    ```
3. Execute the state machine to see whether it follows the correct order of functions and give the right output. 

### Orchestrate data processing pipeline
Now that  we have two lambda functions connected using a step function in AWS, we have successfully orchestrated our data processing pipeline. 


## Srceenshots

### S3 buckets
![alt text](assets/S3_bucket1.png)
![alt text](assets/S3_bucket2.png)

### AWS Step Functions
![alt text](assets/Step_functions.png)
![alt text](<assets/step_lambda functions.png>)

### Execution Results
![alt text](assets/successful.png)

